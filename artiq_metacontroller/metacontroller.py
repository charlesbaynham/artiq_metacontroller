import asyncio
import logging
import threading
import typing as ty
from sys import exc_info

from artiq.experiment import EnvExperiment
from artiq.language.environment import HasEnvironment
from artiq.master.scheduler import Scheduler

from .models import Monitor
from .models import MonitorOutcome


RESTART_DELAY = 10
CHECK_PAUSE_INTERVAL = 1
CANCELLATION_TIMEOUT = 5

logger = logging.getLogger(__name__)


class MetaController(HasEnvironment):
    """
    Metacontroller Experiment to manage several Monitors. This class should be
    inherited by an implementation which registers Monitors using the
    ``register_monitor`` method.
    """

    def __init__(self, *args, **kwargs) -> None:
        self.monitors: ty.List[Monitor] = []
        self.monitor_locks: ty.Dict[str, asyncio.Lock] = {}
        self.artiq_thread_lock = threading.RLock()

        super().__init__(*args, **kwargs)

        # Request the scheduler device here instead of in build. This is
        # naughty, but prevents the user from having to call their build()
        # method with a super() call
        self.setattr_device("scheduler")

    def register_monitor(self, monitor: Monitor):
        """
        Register a new monitor. The monitor will be evaluated whenever its
        Trigger fires, and its action will be performed in response.
        """
        if not isinstance(monitor, Monitor):
            raise TypeError(
                f"This method must be called with a Monitor instance. Got {monitor} which is a {type(monitor)} instead"
            )

        self.monitors.append(monitor)
        if monitor.group is not None and monitor.group not in self.monitor_locks:
            self.monitor_locks[monitor.group] = None

    def run(self):
        asyncio.run(self.main())

    async def main(self):
        self.__inputs_interface = self.__build_inputs()
        self.__outputs_interface = self.__build_outputs()

        self.__monitor_tasks: ty.Dict[Monitor, asyncio.Task] = {}

        # For each monitor, schedule a task to run it
        for monitor in self.monitors:
            logger.info(
                "Launching monitor %s in group %s",
                monitor.__class__.__name__,
                monitor.group,
            )
            coro = self.__generate_monitor_task(monitor)
            self.__monitor_tasks[monitor] = asyncio.create_task(coro())

        # Schedule a task to monitor the monitors, restarting them if they fail
        self.__monitor_monitors_task = asyncio.create_task(self.__monitor_monitors())

        # Finally, schedule a task to listen for ARTIQ cancellation events
        self.__artiq_cancellation_task = asyncio.create_task(
            self.__listen_for_cancellation()
        )

        # If the cancellation task completes, cancel all others
        await self.__artiq_cancellation_task
        await self.__cancel_all_tasks()

    async def __cancel_all_tasks(self):
        all_tasks = [
            task for task in asyncio.all_tasks() if task is not asyncio.current_task()
        ]

        logger.debug("Cancelling all tasks:")
        logger.debug(all_tasks)

        if not all_tasks:
            logger.warning("No tasks to cancel!")
        else:
            gathered_tasks = asyncio.gather(*all_tasks, return_exceptions=True)
            gathered_tasks.cancel()
            try:
                await asyncio.wait_for(gathered_tasks, timeout=CANCELLATION_TIMEOUT)
            except asyncio.CancelledError:
                logger.debug("Cancellation complete")

    async def __listen_for_cancellation(self) -> None:
        """
        Listen for cancellation by the ARTIQ scheduler. If it occurs, return.
        Check every CHECK_PAUSE_INTERVAL seconds.
        """
        self.scheduler: Scheduler

        while True:
            await asyncio.sleep(CHECK_PAUSE_INTERVAL)
            with self.artiq_thread_lock:
                if self.scheduler.check_pause():
                    return

    async def __monitor_monitors(self):
        """
        Watch the list of monitor tasks, and restart any that fail
        """

        if not self.__monitor_tasks:
            logger.warning("No monitors registered! Not starting the monitor monitor")
            return

        async def mm():
            while True:
                logger.debug("(Re)starting monitor monitor")
                await asyncio.wait(
                    list(self.__monitor_tasks.values()),
                    return_when=asyncio.FIRST_COMPLETED,
                )

                logger.error("Monitor monitor detected a failed monitor")

                # if any did, loop through all of them and make sure they're all still
                # running. At least one isn't though: restart it with a delay
                for monitor, task in self.__monitor_tasks.items():
                    if task.done():
                        # This task has ended. Why?

                        if task.cancelled():  # ...it was cancelled
                            logging.info(
                                f"Monitor {monitor.__class__.__name__} was cancelled"
                            )
                        else:
                            if task.exception():  # ...it failed with an exception
                                logger.error(
                                    "Monitor %s exited with exception. Restarting in %.0fs",
                                    monitor.__class__.__name__,
                                    RESTART_DELAY,
                                    exc_info=task.exception(),
                                )
                            else:  # ...it returned with no exception
                                logger.error(
                                    "Monitor %s exited with output %s. Restarting in %.0fs",
                                    monitor.__class__.__name__,
                                    task.result(),
                                    RESTART_DELAY,
                                )

                        new_monitor_task = self.__generate_monitor_task(
                            monitor, delay=RESTART_DELAY
                        )
                        self.__monitor_tasks[monitor] = asyncio.create_task(
                            new_monitor_task()
                        )

        while True:
            t = asyncio.create_task(mm())

            outcome = await asyncio.gather(t, return_exceptions=True)
            logger.critical("Monitor monitor task failed! Outcome was: %s", outcome)
            logger.info("Restarting monitor monitor")

            await asyncio.sleep(RESTART_DELAY)

    def __generate_monitor_task(self, monitor: Monitor, delay=0):
        """
        Generates an async function which will run the passed monitor.

        This function will never exit (unless there is an error). It should be
        scheduled as an asyncio task.
        """

        async def run_this_monitor():
            if delay:
                await asyncio.sleep(delay)

            while True:
                logger.debug("(%s) Awaiting trigger", monitor.__class__.__name__)
                await monitor.trigger.wait()
                logger.debug(
                    "(%s) Trigger complete: obtaining lock for group %s",
                    monitor.__class__.__name__,
                    monitor.group,
                )

                if monitor.group is not None:
                    if self.monitor_locks[monitor.group] is None:
                        logger.debug("Making asyncio.Lock for group %s", monitor.group)
                        self.monitor_locks[monitor.group] = asyncio.Lock()

                    lock = self.monitor_locks[monitor.group]
                else:
                    lock = asyncio.Lock()

                async with lock:
                    logger.debug(
                        "(%s) Lock obtained. Evaluating...",
                        monitor.__class__.__name__,
                    )

                    outcome, info = await monitor.evaluate(self.__inputs_interface)

                    logger.debug(
                        "(%s) Outcome = %s. Performing action...",
                        monitor.__class__.__name__,
                        outcome,
                    )

                    if outcome == MonitorOutcome.OK:
                        await monitor.do_ok(info, self.__outputs_interface)
                    elif outcome == MonitorOutcome.BAD:
                        await monitor.do_bad(info, self.__outputs_interface)
                    elif outcome == MonitorOutcome.NO_INFO:
                        await monitor.do_no_info(info, self.__outputs_interface)
                    elif outcome == MonitorOutcome.ERROR:
                        await monitor.do_error(info, self.__outputs_interface)

                    logger.debug(
                        "(%s) Action complete. Restarting",
                        monitor.__class__.__name__,
                    )

        return run_this_monitor

    def __build_inputs(self):
        """
        Build an object which will be passed to any Monitors' ``evaluate``
        functions. This object exposes functionality by which Monitors can
        interact with the world.

        By passing this object into Monitors we separate concerns and keep
        things easy to test (see Dependency Injection).

        For now, "this object" will be a dict of str -> Callable, which can be
        used to access a given resource.
        """

        from .io import bind_inputs

        return bind_inputs(asyncio.get_event_loop(), self)

    def __build_outputs(self):
        """
        Build an object which will be passed to any Monitors' ``do_xxx``
        functions. This object exposes functionality by which Monitors can
        interact with the world.

        By passing this object into Monitors we separate concerns and keep
        things easy to test (see Dependency Injection).

        For now, "this object" will be a dict of str -> Callable, which can be
        used to access a given resource.
        """

        from .io import bind_outputs

        return bind_outputs(asyncio.get_event_loop(), self)
