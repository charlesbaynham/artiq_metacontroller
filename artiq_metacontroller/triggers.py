"""
Triggers

This module contains some Triggers which can be used to trigger Monitors. These
are probably enough for most purposes, however you can use them as a template to
create your own if you want something more complex.
"""
import asyncio
import time

from .models import Trigger


class IntervalTrigger(Trigger):
    """
    Trigger after a given interval has elapsed.

    Note that the timer starts after all a monitor's evaluate() and do() methods
    have completed. This means that if the interval is e.g. 30s, a Monitor using
    this Trigger will wait for 30s, then evaluate, then do actions, then wait
    another 30s. The evaluation will therefore happen less often than 30s,
    depending on how long the evaluation and actions take.
    """

    def __init__(self, interval) -> None:
        self.interval = interval
        super().__init__()

    async def wait(self):
        await asyncio.sleep(self.interval)


class RegularTrigger(Trigger):
    """
    Trigger every ``interval`` seconds

    Unlike the IntervalTrigger, the RegularTrigger will always ensure that subsequent triggers are spaced by the correct amount of time, regardless of the amount of time taken for the Monitor to evaluate and do its actions. If ``interval`` is shorter than the total computation time, the monitor will be fired again immediately.
    """

    def __init__(self, interval) -> None:
        self.interval = interval
        self.t_next = time.time() + interval

        super().__init__()

    async def wait(self):
        t_now = time.time()

        if t_now >= self.t_next:
            delay = 0
            self.t_next = t_now  # To prevent multiple triggerings building up
        else:
            delay = self.t_next - t_now

        await asyncio.sleep(delay)
        self.t_next += self.interval
