"""
Metacontroller models

This module defines the superclasses used by Monitors. Specifically, subclasses
of Monitor represent a monitor which checks some condition and possibly takes
some action in response. Each Monitor requires a (subclass of) Trigger to define
when its check should be run.

"""
import asyncio
import typing as ty
from enum import Enum


class MonitorOutcome(Enum):
    """
    An enum to represent the possible outcomes of a Monitor's evaluation.
    """

    OK = "OK"
    BAD = "BAD"
    NO_INFO = "NO_INFO"
    ERROR = "ERROR"


class Trigger:
    """
    Provides a wait() method to determine when Monitors should be triggered. A
    simple example of a Trigger just waits for some number of seconds before
    triggering, or waits until a certain time. More complex Triggers are also
    possible.
    """

    async def wait():
        """
        Wait until the monitor's evaluation should be triggered, then return.
        This method must be overridden by child classes and must wait
        asynchronously.
        """
        raise NotImplementedError


class Monitor:
    """
    An interface class to implement a monitor for the metacontroller.

    Monitors must inherit from this class. Monitors:

    * Respond to Triggers
    * When triggered, evaluate a condition based on Inputs
    * Based on the results of the evaluation, optionally perform actions using Outputs

    Examples of Triggers include:

    * Time intervals (i.e. trigger every x seconds)

    Examples of Inputs include:

    * Reading from ARTIQ datasets
    * Reading from an InfluxDB (or other) database
    * Reading the state of the ARTIQ scheduler
    * Getting the output of an Experiment (triggered via the scheduler, accessed via datasets)

    Examples of Outputs include:

    * Writing to ARTIQ datasets
    * Writing to an InfluxDB (or other) database
    * Scheduling an Experiment
    * Scheduling an Experiment, waiting for its conclusion and then taking further action if required

    :ivar group: Group of this Monitor (str or None). If present, only run evaluations and actions of one member of the group at a time
    :ivar trigger: Trigger for this Monitor. Determine when this Monitor's evaluation is performed
    """

    group: ty.Optional[str] = None
    trigger: Trigger = None

    def __init__(self, group=None) -> None:
        """
        Child classes must ensure that self.trigger is an instance of a Trigger
        and that self.group is set to either a string or None.

        This can be done at the class level or at the constructor level,
        depending on your needs.
        """

        if group:
            self.group = group

        if (
            not hasattr(self, "trigger")
            or not isinstance(self.trigger, Trigger)
            or not issubclass(self.trigger.__class__, Trigger)
        ):
            raise ValueError(
                "This Monitor must be created with a Trigger stored at self.trigger"
            )

        if not hasattr(self, "group") or (
            not isinstance(self.group, str) and self.group is not None
        ):
            raise ValueError(
                "Monitors may be part of groups, but self.group must be either a string or None"
            )

    async def evaluate(self, inputs) -> ty.Tuple[MonitorOutcome, ty.Any]:
        """
        Perform the check that this Monitor is monitoring.

        For example, check that the fluorescence of an ion has not dropped to
        zero over the last x seconds. To do this, this method has access to the
        ``inputs`` which can be used to interact with the world.
        """
        raise NotImplementedError

    async def do_ok(self, info: ty.Any, outputs) -> None:
        pass

    async def do_bad(self, info: ty.Any, outputs) -> None:
        pass

    async def do_no_info(self, info: ty.Any, outputs) -> None:
        pass

    async def do_error(self, info: ty.Any, outputs) -> None:
        pass
