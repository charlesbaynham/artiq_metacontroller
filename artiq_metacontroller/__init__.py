from ._version import get_versions

__version__ = get_versions()["version"]
del get_versions

from .models import Trigger, MonitorOutcome, Monitor
from .metacontroller import MetaController

__all__ = ["Trigger", "MonitorOutcome", "Monitor", "MetaController"]

from . import _version

__version__ = _version.get_versions()["version"]
