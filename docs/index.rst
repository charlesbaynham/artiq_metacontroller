Documentation for artiq_metacontroller
======================================


.. include:: ../readme.rst


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   autogen/modules
