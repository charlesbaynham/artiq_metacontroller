README
======

.. note::
    See https://charlesbaynham.gitlab.io/artiq_metacontroller/ for a hosted version
    of these docs, or download the pdf from the artifacts associated with this
    project.

An ARTIQ Experiment which is intended to run continuously in a separate
pipeline, performing regular / triggers tasks. These tasks can interact with the
world by triggering more Experiments to be run, waiting for their results,
reading / writing ARTIQ datasets or reading / writing an InfluxDB database.

Tasks can be triggered at regular time intervals, at particular times, or when
an arbitary condition evaluated on a particular dataset evaluates as true. An
interface to define custom triggers is also provided, for arbitary triggering
conditions.

Crucially, tasks *cannot* access devices directly: all device access must be via
triggering (probably short-lived) Experiments. This allows you to prevent
clashes caused by simultaneous access of the same device, by using ARTIQ's usual
scheduler features to put your short-lived Experiments into the appropriate
pipeline with a higher priority than the currently-running Experiment. This
class does not prevent you from doing this wrong: you are still responsible for
avoiding clashes between running Experiments.

Usage
-----

This documentation is currently very sparse: my apologies. If you want to use
this class, you should probably talk to me.

This is a python package: you need to install it into your virtual environment using::

    pip install --editable .

The ``--editable`` flag is optional, but will allow you to edit this code and
use the results immediately.

If you're already done that and I've just sent you this code, my
double-apologies. The ``example`` folder contains an example of how you could
set a metacontroller system up, and demonstrates some of the key features that
tasks can perform. Try launching ``run.bat`` or ``run.sh`` in this directory
(you need artiq installed) and play with some settings.
