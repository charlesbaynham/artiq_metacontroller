import io
import os
import re

from setuptools import find_packages
from setuptools import setup

import versioneer

setup(
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    name="artiq_metacontroller",
    url="https://gitlab.npl.co.uk/ytterbium/artiq/artiq_metacontroller",
    license="None",
    author="Charles Baynham",
    author_email="charles.baynham@npl.co.uk",
    description="A 'metacontroller' framework to concurrently run monitors and take corrective action in an ARTIQ system",
    packages=find_packages(exclude=("tests",)),
    install_requires=[
        r
        for r in open("requirements.in").read().splitlines()
        if r and not re.match(r"\s*\#", r[0])
    ],
    extras_require={
        "dev": [
            r
            for r in open("requirementsDev.in").read().splitlines()
            if r and not re.match(r"\s*\#", r[0])
        ]
    },
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
)
