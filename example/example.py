import asyncio
import logging
import os
import sys
import typing as ty

from artiq.experiment import BooleanValue
from artiq.language.environment import EnvExperiment
from artiq.language.environment import StringValue
from monitors import *

from artiq_metacontroller import MetaController

# sys.path.append("..")


class MyController(MetaController, EnvExperiment):
    def build(self):
        def add_args_for_monitor(monitor, desc):
            name = monitor.__name__

            self.setattr_argument(
                f"add_{name}", BooleanValue(False), tooltip=desc, group=name
            )
            self.setattr_argument(f"{name}_group", StringValue(""), group=name)
            if getattr(self, f"add_{name}"):
                self.register_monitor(monitor(group=getattr(self, f"{name}_group")))

        add_args_for_monitor(SimpleMonitor, "Always find that everthing is OK")
        add_args_for_monitor(Crasher, "Randomly crash with a 25% probability")
        add_args_for_monitor(
            Recoverer,
            "Randomly go bad with a 25% probability. It'll then spend 4s fixing the problem. ",
        )
        add_args_for_monitor(
            DatasetChecker,
            "Check that a particular dataset is > 0",
        )
        add_args_for_monitor(
            SchedulerChecker,
            "Check the ARTIQ scheduler",
        )
        add_args_for_monitor(
            ExperimentLauncher,
            "Run an experiment and wait for it to finish",
        )

        self.set_default_scheduling(pipeline_name="metacontroller")
