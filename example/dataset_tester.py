import time

from artiq.experiment import EnvExperiment
from artiq.language.core import TerminationRequested
from artiq.language.environment import NumberValue
from artiq.language.environment import StringValue


DATASET_NAME = "some_dataset"


class DatasetWriter(EnvExperiment):
    def build(self):
        self.setattr_argument("dataset_name", StringValue(default="test_dataset"))
        self.setattr_argument("dataset_value", NumberValue(default=0))

        self.setattr_device("scheduler")
        self.set_default_scheduling(pipeline_name="writer")

    def run(self):
        self.set_dataset(self.dataset_name, self.dataset_value, broadcast=True)


class Failer(EnvExperiment):
    def build(self):
        self.setattr_device("scheduler")
        self.set_default_scheduling(pipeline_name="failer")

    def run(self):
        raise RuntimeError("Oops")


# class DatasetReader(EnvExperiment):
#     def build(self):
#         self.setattr_argument("check_delay", NumberValue(default=2, unit="s"))

#         self.setattr_device("scheduler")
#         self.set_default_scheduling(pipeline_name="reader")

#     def run(self):

#         self.set_dataset("num_entries", [], broadcast=True)
#         self.set_dataset("time_taken", [], broadcast=True)

#         while True:

#             t1 = time.time()
#             d = self.get_dataset(DATASET_NAME, archive=False)
#             t2 = time.time()

#             n = len(d)
#             time_taken = t2 - t1

#             print(f"Loading {n} records took {1e3*time_taken:.3f}ms")

#             self.append_to_dataset("num_entries", n)
#             self.append_to_dataset("time_taken", time_taken)

#             time.sleep(self.check_delay)

#             try:
#                 self.scheduler.pause()
#             except TerminationRequested:
#                 return


# class DatasetMassiveWriter(EnvExperiment):
#     def build(self):
#         self.setattr_argument(
#             "num", NumberValue(default=1e4, scale=1, ndecimals=0, step=100, type="int")
#         )
#         self.setattr_device("scheduler")

#         self.set_default_scheduling(pipeline_name="writer")

#     def run(self):
#         from random import random

#         data = [random() for _ in range(self.num)]

#         self.set_dataset(DATASET_NAME, data, broadcast=True)
