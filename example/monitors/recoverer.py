import asyncio
import typing as ty

from artiq_metacontroller import Monitor
from artiq_metacontroller import MonitorOutcome
from artiq_metacontroller.triggers import RegularTrigger


class Recoverer(Monitor):
    trigger = RegularTrigger(4)

    async def do_ok(self, info: ty.Any, outputs) -> None:
        print("(Recoverer) Normal operation: we're all good")

    async def do_bad(self, info: ty.Any, outputs) -> None:
        print("(Recoverer) detected 'bad' condition: trying to fix it now...")

        for _ in range(10):
            print("(Recoverer) trying...")
            await asyncio.sleep(2)

        print("(Recoverer) 'bad' condition resolved! Back to work")

    async def evaluate(self, inputs) -> ty.Tuple[MonitorOutcome, ty.Any]:
        from random import random

        rand = random()

        print(f"(Recoverer) evaluate() called and rand = {rand:.2f}")

        if rand < 0.25:
            return MonitorOutcome.BAD, None
        else:
            return MonitorOutcome.OK, None
