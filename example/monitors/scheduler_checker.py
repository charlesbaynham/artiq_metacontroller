import logging
import typing as ty

from artiq_metacontroller import Monitor
from artiq_metacontroller import MonitorOutcome
from artiq_metacontroller.triggers import IntervalTrigger


class SchedulerChecker(Monitor):
    trigger = IntervalTrigger(4)

    async def evaluate(self, inputs) -> ty.Tuple[MonitorOutcome, ty.Any]:
        from pprint import pformat

        print("(SchedulerChecker) Checking schedule")

        scheduler = inputs["scheduler"]
        status = await scheduler.get_status()

        logging.info(f"(SchedulerChecker) scheduler.get_status() = \n{pformat(status)}")

        return MonitorOutcome.OK, status
