import os
import typing as ty

from artiq.experiment import EnvExperiment

from artiq_metacontroller import Monitor
from artiq_metacontroller import MonitorOutcome
from artiq_metacontroller.triggers import IntervalTrigger


class DoSimpleExperiment(EnvExperiment):
    def build(self):
        pass

    def run(self):
        print("I'm an experiment! I could have done anything")


class ExperimentLauncher(Monitor):
    trigger = IntervalTrigger(4)

    async def evaluate(self, inputs) -> ty.Tuple[MonitorOutcome, ty.Any]:
        return MonitorOutcome.OK, None

    async def do_ok(self, info: ty.Any, outputs) -> None:
        run_and_wait = outputs["run_and_wait"]

        print("Launching experiment...")

        await run_and_wait(__file__, "DoSimpleExperiment", "alternate_pipeline")

        print("The experiment has finished")
