import typing as ty

from artiq_metacontroller import Monitor
from artiq_metacontroller import MonitorOutcome
from artiq_metacontroller.triggers import IntervalTrigger


class Crasher(Monitor):
    trigger = IntervalTrigger(4)

    async def do_ok(self, info: ty.Any, outputs) -> None:
        print("(Crasher) Normal operation: we're all good")

    async def do_error(self, info: ty.Any, outputs) -> None:
        print("(Crasher) Oh no, I'm about to fail!")
        raise RuntimeError("Something bad happened that I didn't expect")

    async def evaluate(self, inputs) -> ty.Tuple[MonitorOutcome, ty.Any]:
        from random import random

        rand = random()

        print(f"(Crasher) evaluate() called and rand = {rand:.2f}")

        if rand < 0.25:
            return MonitorOutcome.ERROR, None
        else:
            return MonitorOutcome.OK, None
