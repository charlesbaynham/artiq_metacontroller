import typing as ty

from artiq_metacontroller import Monitor
from artiq_metacontroller import MonitorOutcome
from artiq_metacontroller.triggers import IntervalTrigger


class SimpleMonitor(Monitor):
    trigger = IntervalTrigger(3)

    async def do_ok(self, info: ty.Any, outputs) -> None:
        print(f"(SimpleMonitor) I did something because the monitor was OK.")

    async def evaluate(self, inputs) -> ty.Tuple[MonitorOutcome, ty.Any]:
        print(f"(SimpleMonitor) evaluate() called")

        return MonitorOutcome.OK, None
