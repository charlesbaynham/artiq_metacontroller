import typing as ty

from artiq_metacontroller import Monitor
from artiq_metacontroller import MonitorOutcome
from artiq_metacontroller.triggers import IntervalTrigger

TEST_DATASET = "test_dataset"


class DatasetChecker(Monitor):
    trigger = IntervalTrigger(4)

    async def evaluate(self, inputs) -> ty.Tuple[MonitorOutcome, ty.Any]:
        print("(DatasetChecker) Looking for dataset %s" % TEST_DATASET)
        get_dataset = inputs["get_dataset"]
        try:
            data = await get_dataset(TEST_DATASET)
        except KeyError:
            return MonitorOutcome.NO_INFO, None

        print(f"(DatasetChecker) dataset {TEST_DATASET} = {data}")

        if data > 0:
            return MonitorOutcome.OK, data
        else:
            return MonitorOutcome.BAD, data

    async def do_bad(self, info, outputs):
        print(f"(DatasetChecker) Dataset '{TEST_DATASET}' BAD: {info}")

    async def do_ok(self, info, outputs):
        print(f"(DatasetChecker) Dataset '{TEST_DATASET}' OK: {info}")

    async def do_no_info(self, info, outputs):
        print(f"(DatasetChecker) Dataset '{TEST_DATASET}' NO_INFO (not found)")
